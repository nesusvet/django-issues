# Yet another issue tracker for django

### Kickstart

Создать новый django-проект по шаблону
```
curl https://bitbucket.org/nesusvet/django-issues/raw/5384ffcb80df7f211c68e58734d6b8d7dd0b0cd0/kickstart.sh | bash
```

### Install

Установить python-пакет из git-репозитория
```
pip install -e git+https://git@bitbucket.org/nesusvet/django-issues.git#egg=django-issues
```

Добавить приложение в список установленных
```
INSTALLED_APPS = (
    ...
    'django_issues',
    ...
)
```

Выполнить миграции для создания таблиц в БД
```
python manage.py migrate django_issues
```

Подключить urls приложения в проект
```
urlpatterns = patterns('',
   ...
   url(r'^issues/', include('django_issues.urls')),
   ...
)
```
