# ~*~ coding: utf-8 ~*~
from django.conf import settings
from django.http import Http404
from django.shortcuts import get_object_or_404, redirect
from django.contrib.auth.decorators import login_required
from django.utils.decorators import method_decorator
from django.views.generic import ListView, DetailView, CreateView

from .models import Issue
from .forms import *


class LoginRequiredMixin(object):
    """
    A login required mixin for use with class based views. This Class is a light wrapper around the
    `login_required` decorator and hence function parameters are just attributes defined on the class.

    Due to parent class order traversal this mixin must be added as the left most
    mixin of a view.

    The mixin has exaclty the same flow as `login_required` decorator:

        If the user isn't logged in, redirect to settings.LOGIN_URL, passing the current
        absolute path in the query string. Example: /accounts/login/?next=/polls/3/.

        If the user is logged in, execute the view normally. The view code is free to
        assume the user is logged in.

    **Class Settings**
        `redirect_field_name - defaults to "next"
        `login_url` - the login url of your site

    """
    redirect_field_name = 'next'
    login_url = settings.LOGIN_URL

    @method_decorator(login_required(redirect_field_name=redirect_field_name, login_url=login_url))
    def dispatch(self, request, *args, **kwargs):
        return super(LoginRequiredMixin, self).dispatch(request, *args, **kwargs)


class IssueList(LoginRequiredMixin, ListView):
    model = Issue
    context_object_name = 'issues'

    def get_context_data(self, **kwargs):
        context = super(IssueList, self).get_context_data(**kwargs)
        context.update(search_form=IssueSelector())
        return context


class SearchView(IssueList):
    form_class = IssueSelector

    def post(self, request, *args, **kwargs):
        form = self.form_class(request.POST)
        if not form.is_valid():
            return redirect('issue_list')

        object_list = form.get_objects().order_by('-modified_ts')
        is_empty = not object_list.exists()
        if is_empty:
            raise Http404(u'Ничего не найдено')

        context = self.get_context_data(object_list=object_list)
        return self.render_to_response(context)


class IssueDetail(LoginRequiredMixin, DetailView):
    model = Issue
    context_object_name = 'issue'

    def get(self, request, *args, **kwargs):
        self.user = request.user
        return super(IssueDetail, self).get(request, *args, **kwargs)

    def get_context_data(self, **kwargs):
        context = super(IssueDetail, self).get_context_data(**kwargs)
        initial = {'author': self.user, 'issue': self.object}
        form = IssueCommentForm(initial=initial)
        context.update(
            title=self.object.title,
            new_comment_form=form,
        )
        return context


@login_required
def post_comment(request, **kwargs):
    issue = get_object_or_404(Issue, id=kwargs.get('pk'))
    if request.method == 'POST':
        form = IssueCommentForm(request.POST)
        if form.is_valid():
            if form.cleaned_data['make_done']:
                issue.close()
            form.save()

    return redirect(issue.get_absolute_url())
