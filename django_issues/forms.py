# -*- coding: utf-8 -*-
from django import forms
from django.db.models import Q

from .models import Issue, IssueComment


class IssueCommentForm(forms.ModelForm):
    make_done = forms.BooleanField(label=u'Закрыть задачу', required=False)

    class Meta:
        model = IssueComment
        fields = ('issue', 'author', 'text')
        widgets = {
            'text': forms.Textarea(attrs={'cols': 80, 'rows': 5}),
            'author': forms.HiddenInput(),
            'issue': forms.HiddenInput(),
        }


class IssueSelector(forms.Form):
    query = forms.CharField(label='Поиск', widget=forms.TextInput(attrs={'size': 25}))
    queryset = Issue.objects

    @staticmethod
    def build_query(query):
        return (
            Q(title__icontains=query) |
            Q(text__icontains=query)
        )

    def get_objects(self):
        query = self.cleaned_data.get('query')
        queryset = super(IssueSelector, self).get_objects()
        return queryset.filter(self.build_query(query))


__all__ = (
    'IssueCommentForm',
    'IssueSelector',
)
