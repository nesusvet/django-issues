# -*- coding: utf-8 -*-
"""
Based on http://djangosnippets.org/snippets/28/
"""

import datetime

from django.db import models
from django.core.urlresolvers import reverse
from django.contrib.auth.models import User

STATUSES = (
    ('opened', u'Открыт'),
    ('running', u'В работе'),
    ('paused', u'Пауза'),
    ('rejected', u'Отклонен'),
    ('done', u'Решен'),
    ('testing', u'Проверяется'),
    ('closed', u'Закрыт'),
    ('feedback', u'Ожидается отзыв'),
)
DEFAULT_STATUS = 'opened'
CLOSED_STATUS = 'closed'
CLOSED_STATUSES = (
    CLOSED_STATUS,
    'rejected',
)

PRIORITIES = (
    ('3-high', u'Высокий'),
    ('5-normal', u'Нормальный'),
    ('7-low', u'Низкий'),
)
DEFAULT_PRIORITY = '5-normal'

ISSUE_QUEUES = (
    ('common', u'Общие задачи'),
    ('admin', u'Административные'),
)
DEFAULT_ISSUE_QUEUE = 'common'

ISSUE_TYPES = (
    ('error', u'Ошибка'),
    ('task', u'Задача'),
    ('improvement', u'Улучшение'),
    ('support', u'Поддержка'),
)
DEFAULT_ISSUE_TYPE = 'task'


class IssueManager(models.Manager):

    def opened(self):
        return self.get_queryset().exclude(status__in=CLOSED_STATUSES).order_by('-created_ts')

    def closed(self):
        return self.get_queryset().filter(status__in=CLOSED_STATUSES).order_by('-modified_ts')


class Issue(models.Model):
    title = models.CharField(u'Заголовок', max_length=255)
    description = models.TextField(u'Описание', help_text=u'поддерживается markdown', blank=True, null=True)

    issue_queue = models.CharField(u'Проект', max_length=20, choices=ISSUE_QUEUES, default=DEFAULT_ISSUE_QUEUE)
    issue_type = models.CharField(u'Трекер', max_length=20, choices=ISSUE_TYPES, default=DEFAULT_ISSUE_TYPE)

    status = models.CharField(u'Статус', max_length=20, choices=STATUSES, default=DEFAULT_STATUS)
    priority = models.CharField(u'Приоритет', max_length=20, choices=PRIORITIES, default=DEFAULT_PRIORITY)

    author = models.ForeignKey(User, verbose_name=u'Отправитель', related_name="issues_by")
    assigned_to = models.ForeignKey(
        User, verbose_name=u'Назначено', related_name="issues_to", blank=True, null=True,
    )

    created_ts = models.DateTimeField(u'Создана', auto_now_add=True, editable=False)
    deadline = models.DateField(u'Срок исполнения', blank=True, null=True)
    modified_ts = models.DateTimeField(u'Обновлена', auto_now=True, editable=False)
    modified_by = models.ForeignKey(
        User, verbose_name=u'Последний', related_name='last_modified_issues', editable=False, null=True,
    )

    objects = IssueManager()

    def is_closed(self):
        return self.status in CLOSED_STATUSES

    def close(self):
        self.status = CLOSED_STATUS
        self.save()

    def update(self):
        self.modified_ts = datetime.datetime.now()
        self.save()

    def get_absolute_url(self):
        return reverse("issue_detail", kwargs={'pk': self.id})

    def __unicode__(self):
        return unicode(self.title)

    class Meta:
        ordering = ('priority', '-modified_ts', '-created_ts')
        verbose_name = u'Задача'
        verbose_name_plural = u'Задачи'
        get_latest_by = 'created_ts'


class IssueComment(models.Model):
    author = models.ForeignKey(User, verbose_name=u'Автор')
    created_ts = models.DateTimeField(auto_now_add=True, verbose_name=u'Время создания', editable=False)

    issue = models.ForeignKey(Issue, related_name="comments", verbose_name=u'Задача')
    parent = models.ForeignKey('self', related_name='children', null=True, editable=False)
    text = models.TextField(
        u'Текст комментария', help_text=u'поддерживается markdown',
        default=u'введите текст нового комментария',
    )

    def __unicode__(self):
        return "%s - %s : %s" % (unicode(self.created_ts), self.author.get_full_name(), self.text)

    class Meta:
        ordering = ('issue', 'created_ts')
        verbose_name = u'Комментарии к задаче'
        verbose_name_plural = u'Комментарии к задачам'
        get_latest_by = "created_ts"
