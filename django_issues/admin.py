# -*- coding: utf-8 -*-
from django.contrib import admin

from .models import *


class IssueCommentInline(admin.TabularInline):
    model = IssueComment
    extra = 1


class IssueAdmin(admin.ModelAdmin):
    list_display = ('title', 'status', 'priority', 'author', 'assigned_to', 'created_ts', 'modified_ts')
    list_filter = ('issue_queue', 'issue_type', 'priority', 'status', 'assigned_to')
    date_hierarchy = 'created_ts'
    search_fields = ('title', 'description')
    inlines = (IssueCommentInline, )


admin.site.register(Issue, IssueAdmin)
admin.site.register(IssueComment)
