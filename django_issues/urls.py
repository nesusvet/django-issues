from django.conf.urls import *

from .views import IssueList, IssueDetail, SearchView


urlpatterns = patterns('django_issues.views',
    url(r'^$', IssueList.as_view(), name='issue_list'),
    url(r'^(?P<pk>\d+)/$', IssueDetail.as_view(), name='issue_detail'),
    url(r'^(?P<pk>\d+)/comment/$', 'post_comment'),
    url(r'^search/$', SearchView.as_view(), name='issue_search'),
)
