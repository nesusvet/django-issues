#!/usr/bin/env bash

virtualenv env
. env/bin/activate

PACKAGE_URL="git+https://git@bitbucket.org/nesusvet/django-issues.git#egg=django-issues"
pip install -e ${PACKAGE_URL}

PROJECT_NAME=django_project
django-admin startproject ${PROJECT_NAME}
